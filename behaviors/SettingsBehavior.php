<?php
namespace Giveandgo\Utils\Behaviors;

use System\Behaviors\SettingsModel;

class SettingsBehavior extends SettingsModel
{

    /**
     * Helper for getSettingsValue, intended as a static method
     */
    public function get($key, $default = null)
    {
        if ($this->instance()->isClassExtendedWith('RainLab.Translate.Behaviors.TranslatableModel') && $this->instance()->isTranslatable($key)) {
            return $this->instance()->getAttributeTranslated($key);
        }

        if ($this->model->hasRelation($key)) {
            return $this->instance()->$key;
        }

        return $this->instance()->getSettingsValue($key, $default);
    }
}
