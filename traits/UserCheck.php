<?php

namespace Giveandgo\Utils\Traits;

use Auth;

trait UserCheck
{

    /**
     * @var \RainLab\User\Models\User Cached user
     */
    protected $authUser;

    /**
     * Get user from Auth if not cached
     * @return \RainLab\User\Models\User
     */
    protected function getUser()
    {
        if ($this->authUser !== null) {
            return $this->authUser;
        }

        return $this->authUser = Auth::getUser();
    }
}
