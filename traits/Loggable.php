<?php

namespace Giveandgo\Utils\Traits;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Symfony\Component\HttpFoundation\File\File as FileObject;
use System\Models\File as SystemFile;

trait Loggable
{

    /*
     * Constructor
     */
    /**
     * @return null
     */
    public static function bootLoggable()
    {
        static::extend(function ($model) {
            /*
             * Define relationships
             */
            $model->attachOne['logfile'] = [
                SystemFile::class,
            ];

            // Extend all backend form usage
            \Event::listen('backend.form.extendFields', function ($widget) use ($model) {

                $modelClass = get_class($model);

                // Only for the User model
                if (!$widget->model instanceof $modelClass) {
                    return;
                }

                // Add an extra birthday field
                $widget->addTabFields([
                    $model->getLoggingColumn() => [
                        'label' => 'Is logging',
                        'type'  => 'switch',
                        'tab'   => 'Logging',
                    ],
                    'logfile'                  => [
                        'label'      => 'Log file',
                        'type'       => 'fileupload',
                        'mode'       => 'file',
                        'readOnly'   => true,
                        'useCaption' => false,
                        'trigger'    => [
                            'action'    => 'show',
                            'field'     => $model->getLoggingColumn(),
                            'condition' => 'checked',
                        ],
                        'tab'        => 'Logging',
                    ],
                ]);

            });
        });

    }

    /**
     * Get the name of the "sort order" column.
     * @return string
     */
    public function getLoggingColumn()
    {
        return defined('static::IS_LOGGING') ? static::IS_LOGGING : 'is_logging';
    }

    /**
     * Log an informational message to the logs.
     *
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function info($message)
    {
        $this->writeLog(__FUNCTION__, $message);
    }

    /**
     * Log a debug message to the logs.
     *
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function debug($message)
    {
        $this->writeLog(__FUNCTION__, $message);
    }

    /**
     * Log a message to the logs.
     *
     * @param  string $level
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function log($level, $message)
    {
        $this->writeLog($level, $message);
    }

    /**
     * @param $level
     * @param $message
     */
    protected function writeLog($level, $message, $updateFileSize = false)
    {
        $loggingColumn = $this->getLoggingColumn();

        if (!$this->$loggingColumn) {
            return;
        }

        if (array_get($this->attributes, $loggingColumn)) {

            if (!$this->logfile) {

                $filename = str_slug(get_class($this)) . '_' . $this->id . '_log.txt';
                $file     = new SystemFile();

                $file->fromData('', $filename);
                $file->save();

                $this->logfile()->add($file);
            }
        }

        $logFilePath = $this->logfile->getLocalPath();

        $now = Carbon::now();

        $level   = " [$level]: " . PHP_EOL;
        $message = $this->formatMessage($message);
        $message = $now . $level . $message . PHP_EOL . PHP_EOL;

        \File::append($logFilePath, $message);

        if ($updateFileSize) {
            $logFileObject = new FileObject($logFilePath);

            $this->logfile->update([
                'file_size' => $logFileObject->getSize(),
            ]);
        }
    }

    /**
     * Format the parameters for the logger.
     *
     * @param  mixed   $message
     * @return mixed
     */
    protected function formatMessage($message)
    {
        if (is_array($message)) {
            return var_export($message, true);
        } elseif ($message instanceof Jsonable) {
            return $message->toJson();
        } elseif ($message instanceof Arrayable) {
            return var_export($message->toArray(), true);
        }

        return $message;
    }

}
