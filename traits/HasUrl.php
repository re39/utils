<?php

namespace Giveandgo\Utils\Traits;

use Cms\Classes\Page as CmsPage;
use Cms\Classes\Theme;

trait HasUrl
{
    /**
     * @var mixed
     */
    public $url;

    /**
     * @var array Define url params and attributes to fill them
     *
     * protected $urlParams = [];
     */

    /**
     * @var array Component name to look for in pages for url generation
     *
     * protected $component = '';
     */

    /**
     * Get actual values for url parameters
     * @return array Url parameters
     */
    protected function getUrlParams()
    {
        $params = [];

        foreach ($this->urlParams as $key => $value) {
            if (array_key_exists($value, $this->attributes)) {

                if (!is_string($key)) {
                    $key = $value;
                }

                $params[$key] = $this->$value;
            }
        }

        return $params;
    }

    /**
     * Lightweight function to set models URL using page name and controller
     * @param  string                  $pageName
     * @param  \Cms\Classes\Controller $controller
     * @return string                  URL
     */
    public function setUrl($pageName, $controller)
    {
        $params = $this->getUrlParams();

        return $this->url = $controller->pageUrl($pageName, $params);
    }

    /**
     * Heavier url generation function
     * @return string Url
     */
    public function generateUrl($pages = null)
    {
        if (!$pages) {
            $theme = Theme::getActiveTheme();

            $pages = CmsPage::listInTheme($theme, true);
        }

        $cmsPage = null;

        foreach ($pages as $page) {
            if ($page->hasComponent($this->component)) {
                $cmsPage = $page;
                break;
            }
        }

        $url = CmsPage::url($cmsPage->getBaseFileName(), $this->getUrlParams());

        return $this->url = $url;
    }
}
