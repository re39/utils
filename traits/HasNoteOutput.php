<?php

namespace Giveandgo\Utils\Traits;

use Illuminate\Console\OutputStyle;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * HasNoteOutput
 */
trait HasNoteOutput
{
    /**
     * @var array The notes for the current operation.
     */
    protected $notes = [];

    /**
     * @var \Illuminate\Console\OutputStyle
     */
    protected $notesOutput;

    /**
     * @var \Symfony\Component\Console\Helper\ProgressBar;
     */
    protected $bar;

    /**
     * @var mixed
     */
    public $enableOutput = true;

    //
    // Notes
    //

    /**
     * Raise a note event for the migrator.
     * @param  string $message
     * @return self
     */
    protected function note($message, $style = null)
    {
        if ($this->enableOutput) {
            if ($this->notesOutput !== null) {
                $styled = $style ? "<$style>$message</$style>" : $message;

                $this->notesOutput->writeln($styled);
            } else {
                $this->notes[] = $message;
            }
        }

        return $this;
    }

    /**
     * Get the notes for the last operation.
     * @return array
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Resets the notes store.
     * @return self
     */
    public function resetNotes()
    {
        $this->notesOutput = null;

        $this->notes = [];

        return $this;
    }

    /**
     * Sets an output stream for writing notes.
     * @param  Illuminate\Console\Command $output
     * @return self
     */
    public function setNotesOutput($output)
    {
        $this->notesOutput = $output;

        return $this;
    }

    /**
     * @param $string
     */
    public function error($string)
    {
        $this->note($string, 'error');
    }

    /**
     * @param $string
     */
    public function info($string)
    {
        $this->note($string, 'info');
    }

    /**
     * @param $string
     */
    public function comment($string)
    {
        $this->note($string, 'comment');
    }

    /**
     * @param $string
     */
    public function definitionList(...$list)
    {
        if ($this->notesOutput === null) {
            return null;
        }

        $this->notesOutput->definitionList(...$list);
    }

    /**
     * @param $string
     */
    public function table(array $headers, array $data)
    {
        if ($this->notesOutput === null) {
            return null;
        }

        $this->notesOutput->table($headers, $data);
    }

    /**
     * @param int $count
     */
    public function startBar(int $count)
    {
        if ($this->notesOutput === null) {
            return null;
        }

        $this->bar = $this->notesOutput->createProgressBar($count);

        // $this->bar->setOverwrite(false);
        $this->bar->start();

        return $this->bar;
    }

    /**
     * @return mixed
     */
    public function advanceBar()
    {
        if ($this->notesOutput === null) {
            return null;
        }
        if ($this->bar) {
            $this->bar->advance();
        }

        return $this->bar;
    }

    /**
     * @return mixed
     */
    public function finishBar()
    {
        if ($this->notesOutput === null) {
            return null;
        }
        if ($this->bar) {
            $this->bar->finish();
        }
        $this->notesOutput->newLine();

        return $this->bar;
    }

    /**
     * @return \Illuminate\Console\OutputStyle
     */
    public function makeNotesOutput()
    {
        $output = app()->make(OutputStyle::class, ['input' => new ArgvInput(), 'output' => new ConsoleOutput()]);
        $this->setNotesOutput($output);

        return $this;
    }
}
