<?php

namespace Giveandgo\Utils\Traits;

trait JsonArrayable
{

    /**
     * @return mixed
     */
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();

        /*
         * Jsonable
         */
        foreach ($this->jsonable as $key) {
            if (!array_key_exists($key, $attributes)) {
                continue;
            }

            if (is_array($attributes[$key])) {
                continue;
            }

            $jsonValue = json_decode($attributes[$key], true);
            if (json_last_error() === JSON_ERROR_NONE) {
                $attributes[$key] = $jsonValue;
            }
        }

        return $attributes;
    }
}
