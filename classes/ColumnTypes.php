<?php

namespace Giveandgo\Utils\Classes;

use Lang;
use October\Rain\Database\Model;
use October\Rain\Exception\ApplicationException;
use October\Rain\Html\Helper as HtmlHelper;

/**
 * ColumnTypes
 */
class ColumnTypes
{

    /**
     * @param $value
     * @param $column
     * @param $record
     */
    public static function dropdown($value, $column, $model)
    {

        $attrArray = HtmlHelper::nameToArray($column->columnName);
        $attribute = array_last($attrArray);

        /*
         * Loop the column key parts and build a value.
         * To support relations only the last column should return the
         * relation value, all others will look up the relation object as normal.
         */
        foreach ($attrArray as $key) {
            if ($model instanceof Model && ($key == 'pivot' || $model->hasRelation($key))) {
                $model = $model->{$key};
            }
        }

        if (!$model) {
            return null;
        }

        $methodName = 'get' . studly_case($attribute) . 'Options';
        if (
            !static::objectMethodExists($model, $methodName) &&
            !static::objectMethodExists($model, 'getDropdownOptions')
        ) {
            throw new ApplicationException(Lang::get('backend::lang.field.options_method_not_exists', [
                'model'  => get_class($model),
                'method' => $methodName,
                'field'  => $column->columnName,
            ]));
        }

        if (static::objectMethodExists($model, $methodName)) {
            $fieldOptions = $model->$methodName($value);
        } else {
            $fieldOptions = $model->getDropdownOptions($attribute, $value);
        }

        $value = array_get($fieldOptions, $value ?: 0);

        return $value;
    }

    /**
     * Internal helper for method existence checks.
     *
     * @param  object    $object
     * @param  string    $method
     * @return boolean
     */
    protected static function objectMethodExists($object, $method)
    {
        if (method_exists($object, 'methodExists')) {
            return $object->methodExists($method);
        }

        return method_exists($object, $method);
    }

}
