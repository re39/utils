<?php
namespace Giveandgo\Utils\Classes;

use System\Classes\PluginManager;

class TwigFilters
{
    /**
     * @param  $str
     * @param  $limit
     * @param  $end
     * @return mixed
     */
    public static function gngSubstr($str, $limit, $end = '...')
    {
        if (mb_strlen($str) > $limit) {
            $result = mb_substr($str, 0, $limit) . $end;
        } else {
            $result = $str;
        }

        return $result;
    }

    /**
     * @param $value
     * @param array    $protocols
     * @param array    $attributes
     */
    public static function autoLinkText($value, $protocols = array('http', 'mail'), array $attributes = ['target' => '_blank', 'rel' => 'nofollow'])
    {
        // Link attributes
        $attr = '';
        foreach ($attributes as $key => $val) {
            $attr .= ' ' . $key . '="' . htmlentities($val) . '"';
        }

        $links = array();

        // Extract existing links and tags
        $value = preg_replace_callback('~(<a .*?>.*?</a>|<.*?>)~i', function ($match) use (&$links) {return '<' . array_push($links, $match[1]) . '>';}, $value);

        // Extract text links for each protocol
        foreach ((array) $protocols as $protocol) {
            switch ($protocol) {
                case 'http':
                case 'https':$value = preg_replace_callback('~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) {
                        if ($match[1]) {
                            $protocol = $match[1];
                        }
                        $link = $match[2] ?: $match[3];

                        return '<' . array_push($links, "<noindex><a $attr href=\"$protocol://$link\">$link</a></noindex>") . '>';}, $value);
                    break;
                case 'mail':$value = preg_replace_callback('~([^\s<]+?@[^\s<]+?\.[^\s<]+)(?<![\.,:])~', function ($match) use (&$links, $attr) {return '<' . array_push($links, "<a $attr href=\"mailto:{$match[1]}\">{$match[1]}</a>") . '>';}, $value);
                    break;
                case 'twitter':$value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) {return '<' . array_push($links, "<a $attr href=\"https://twitter.com/" . ($match[0][0] == '@' ? '' : 'search/%23') . $match[1] . "\">{$match[0]}</a>") . '>';}, $value);
                    break;
                default:$value = preg_replace_callback('~' . preg_quote($protocol, '~') . '://([^\s<]+?)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) {return '<' . array_push($links, "<a $attr href=\"$protocol://{$match[1]}\">{$match[1]}</a>") . '>';}, $value);
                    break;
            }
        }

        // Insert all link

        return preg_replace_callback('/<(\d+)>/', function ($match) use (&$links) {return $links[$match[1] - 1];}, $value);
    }

    /**
     * @param  $number
     * @param  $endingArray
     * @param  $includeNum
     * @return mixed
     */
    public static function gngNumEnding($number, $endingArray, $includeNum = false)
    {
        $num    = $number;
        $number = $number % 100;
        if ($number >= 11 && $number <= 19) {
            $ending = $endingArray[2];
        } else {
            $i = $number % 10;
            switch ($i) {
                case (1): $ending = $endingArray[0];
                    break;
                case (2):
                case (3):
                case (4): $ending = $endingArray[1];
                    break;
                default:$ending = $endingArray[2];
            }
        }

        return $includeNum ? ($num . ' ' . $ending) : $ending;
    }

    /**
     * Возвращает сумму прописью
     * @uses morph(...)
     * @author runcore
     */
    public static function gngNum2str($num)
    {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20     = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens    = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit    = array( // Units
            array('копейка', 'копейки', 'копеек', 1),
            array('рубль', 'рубля', 'рублей', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );
        //
        list($rub, $kop) = explode('.', sprintf('%015.2f', floatval($num)));
        $out             = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) {
                // by 3 symbols
                if (!intval($v)) {
                    continue;
                }

                $uk                 = sizeof($unit) - $uk - 1; // unit key
                $gender             = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) {
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3];
                }
                # 20-99
                else {
                    $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3];
                }
                # 10-19 | 1-9
                // units without rub & kop
                if ($uk > 1) {
                    $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
                }

            } //foreach
        } else {
            $out[] = $nul;
        }

        $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]);      // rub
        $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop

        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    /**
     * @param $timestamp
     * @param $format
     * @param $case
     */
    public static function rdate($timestamp, $format, $case = 0)
    {
        if ($timestamp === null) {
            $timestamp = time();
        }

        /**
         * @var string
         */
        static $loc =
            'Январ,ь,я,е,ю,ём,е
    Феврал,ь,я,е,ю,ём,е
    Март, ,а,е,у,ом,е
    Апрел,ь,я,е,ю,ем,е
    Ма,й,я,е,ю,ем,е
    Июн,ь,я,е,ю,ем,е
    Июл,ь,я,е,ю,ем,е
    Август, ,а,е,у,ом,е
    Сентябр,ь,я,е,ю,ём,е
    Октябр,ь,я,е,ю,ём,е
    Ноябр,ь,я,е,ю,ём,е
    Декабр,ь,я,е,ю,ём,е';
        if (is_string($loc)) {
            $months = array_map('trim', explode("\n", $loc));
            $loc    = array();
            foreach ($months as $monthLocale) {
                $cases = explode(',', $monthLocale);
                $base  = array_shift($cases);
                $cases = array_map('trim', $cases);
                $loc[] = array(
                    'base'  => $base,
                    'cases' => $cases,
                );
            }
        }
        $m      = (int) date('n', $timestamp) - 1;
        $F      = $loc[$m]['base'] . $loc[$m]['cases'][$case];
        $format = strtr($format, array(
            'F' => $F,
            'M' => substr($F, 0, 3),
        ));

        return date($format, $timestamp);
    }

    /**
     * Replace \n to paragraphes with <p> tag
     * @param  string   to convert
     * @return string
     */
    public static function nl2p($str)
    {
        $s    = explode("\n", $str);
        $html = '';
        foreach ($s as $val) {
            $html .= "<p>$val</p>";
        }

        return $html;
    }

    /**
     * Check [lugin by name
     * @param  string $sPluginName
     * @return bool
     */
    public static function checkPlugin($sPluginName)
    {
        if (empty($sPluginName)) {
            return false;
        }
        $bResult = PluginManager::instance()->hasPlugin($sPluginName) && !PluginManager::instance()->isDisabled($sPluginName);

        return $bResult;
    }

/**
 * Склоняем словоформу
 * @ author runcore
 */
    public static function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) {
            return $f5;
        }

        $n = $n % 10;
        if ($n > 1 && $n < 5) {
            return $f2;
        }

        if ($n == 1) {
            return $f1;
        }

        return $f5;
    }
}
