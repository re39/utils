<?php
namespace Giveandgo\Utils\FormWidgets;

use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;

/**
 * arraylist Form Widget
 */
class Arraylist extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'elastic_main_arraylist';

    /**
     * @inheritDoc
     */
    public function init()
    {
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('arraylist');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['field'] = $this->formField;
        $this->vars['name']  = $this->formField->getName();
        $value               = $this->getLoadValue();
        $this->vars['value'] = is_array($value) ? implode("\n", $value) : $value;
        $this->vars['model'] = $this->model;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/arraylist.css', 'giveandgo.utils');
        $this->addJs('js/arraylist.js', 'giveandgo.utils');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        if ($this->formField->disabled || $this->formField->hidden) {
            return FormField::NO_SAVE_DATA;
        }

        return explode("\n", $value);
    }
}
