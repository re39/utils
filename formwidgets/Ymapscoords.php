<?php
namespace Giveandgo\Utils\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * ymapscoords Form Widget
 */
class Ymapscoords extends FormWidgetBase
{

    /**
     * {@inheritDoc}
     */
    protected $defaultAlias = 'giveandgo_utils_ymapscoords';

    /**
     * {@inheritDoc}
     */
    public function init()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('ymapscoords');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['field'] = $this->formField;
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
    }

    /**
     * {@inheritDoc}
     */
    public function loadAssets()
    {
        $this->addCss('css/ymapscoords.css', 'giveandgo.utils');
        $this->addJs('js/ymapscoords.js', 'giveandgo.utils');
        $this->addJs('https://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU');
    }

    /**
     * {@inheritDoc}
     */
    public function getSaveValue($value)
    {
        return $value;
    }

}
