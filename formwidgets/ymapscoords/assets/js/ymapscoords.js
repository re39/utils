$(document).ready(function() {


    if ($(".btn.btn-sm.btn-primary.oc-icon-pencil").length > 0) {



        $(".btn.btn-sm.btn-primary.oc-icon-pencil").click(function() {
            setTimeout(function() {
                var customCords = new Array();
                console.log(window.fieldId)
                var currentCord = $("#" + window.fieldId).val();

                if (currentCord == "" || currentCord == null) {
                    var customCords = [54.693079, 20.469566];
                } else {
                    customCords = currentCord.split(",");
                }
                var myMap, myPlacemark, coords;
                ymaps.ready(init);

                function init() {
                    if ($("div").is("#YMapsID")) {
                        var controls;
                        var preview;
                        if ($("#YMapsID").data('preview')) {
                            controls = [];
                            preview = true;
                        } else {
                            controls = ['zoomControl', 'searchControl'];
                            preview = false;
                        }

                        //Определяем начальные параметры карты
                        myMap = new ymaps.Map('YMapsID', {
                            center: [customCords[0], customCords[1]],
                            zoom: 13,
                            controls: controls,
                        });

                        //Определяем элемент управления поиск по карте  
                        var SearchControl = new ymaps.control.SearchControl({ noPlacemark: true });

                        coords = [customCords[0], customCords[1]];

                        //Определяем метку и добавляем ее на карту              
                        myPlacemark = new ymaps.Placemark([customCords[0], customCords[1]], {}, { preset: "twirl#redIcon", draggable: true });

                        myMap.geoObjects.add(myPlacemark);

                        if (!preview) {
                            //Отслеживаем событие перемещения метки
                            myPlacemark.events.add("dragend", function(e) {
                                coords = this.geometry.getCoordinates();
                                savecoordinats(coords);
                            }, myPlacemark);

                            //Отслеживаем событие щелчка по карте
                            myMap.events.add('click', function(e) {
                                coords = e.get('coords');
                                savecoordinats(coords);
                            });

                            //Отслеживаем событие выбора результата поиска
                            SearchControl.events.add("resultselect", function(e) {
                                coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
                                // savecoordinats();
                            });
                        }
                        // 
                        //Ослеживаем событие изменения области просмотра карты - масштаб и центр карты
                        myMap.events.add('boundschange', function(event) {
                            if (event.get('newZoom') != event.get('oldZoom')) {
                                // savecoordinats();
                            }
                            if (event.get('newCenter') != event.get('oldCenter')) {
                                // savecoordinats();
                            }

                        });

                    }



                }



                //Функция для передачи полученных значений в форму
                function savecoordinats(coords) {
                    var new_coords = [coords[0].toFixed(4), coords[1].toFixed(4)];
                    myPlacemark.geometry.setCoordinates(new_coords);
                    document.getElementById(window.fieldId).value = new_coords;

                    /*
                     * Trigger change event (Compatability with october.form.js)
                     */
                     $("#"+window.fieldId).closest('[data-field-name]').trigger('change.oc.formwidget');
                }

            }, 1000);

        });
    } else {
        setTimeout(function() {
            var customCords = new Array();
            var currentCord = $("#" + window.fieldId).val();
            console.log(window.fieldId)
            console.log($("#" + window.fieldId).val())

            if (currentCord == "" || currentCord == null) {
                var customCords = [54.693079, 20.469566];
            } else {
                customCords = currentCord.split(",");
            }
            var myMap, myPlacemark, coords;
            ymaps.ready(init);

            function init() {
                if ($("div").is("#YMapsID")) {

                    var controls;
                    var preview;
                    if ($("#YMapsID").data('preview')) {
                        controls = [];
                        preview = true;
                    } else {
                        controls = ['zoomControl', 'searchControl'];
                        preview = false;
                    }

                    //Определяем начальные параметры карты
                    myMap = new ymaps.Map('YMapsID', {
                        center: [customCords[0], customCords[1]],
                        zoom: 13,
                        controls: controls,
                    });

                    //Определяем элемент управления поиск по карте  
                    var SearchControl = new ymaps.control.SearchControl({ noPlacemark: true });

                    coords = [customCords[0], customCords[1]];

                    //Определяем метку и добавляем ее на карту              
                    myPlacemark = new ymaps.Placemark([customCords[0], customCords[1]], {}, { preset: "twirl#redIcon", draggable: true });

                    myMap.geoObjects.add(myPlacemark);

                    if (!preview) {

                        //Отслеживаем событие перемещения метки
                        myPlacemark.events.add("dragend", function(e) {
                            coords = this.geometry.getCoordinates();
                            savecoordinats(coords);
                        }, myPlacemark);

                        //Отслеживаем событие щелчка по карте
                        myMap.events.add('click', function(e) {
                            coords = e.get('coords');
                            savecoordinats(coords);
                        });

                        //Отслеживаем событие выбора результата поиска
                        SearchControl.events.add("resultselect", function(e) {
                            coords = SearchControl.getResultsArray()[0].geometry.getCoordinates();
                            // savecoordinats();
                        });
                    }
                    // 
                    //Ослеживаем событие изменения области просмотра карты - масштаб и центр карты
                    myMap.events.add('boundschange', function(event) {
                        if (event.get('newZoom') != event.get('oldZoom')) {
                            // savecoordinats();
                        }
                        if (event.get('newCenter') != event.get('oldCenter')) {
                            // savecoordinats();
                        }

                    });

                }



            }



            //Функция для передачи полученных значений в форму
            function savecoordinats(coords) {
                var new_coords = [coords[0].toFixed(4), coords[1].toFixed(4)];
                myPlacemark.geometry.setCoordinates(new_coords);
                document.getElementById(window.fieldId).value = new_coords;
                $("#"+window.fieldId).closest('[data-field-name]').trigger('change.oc.formwidget');
            }

        }, 1000);
    }


});
