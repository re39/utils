<?php

namespace Giveandgo\Utils\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * ymapscoords Form Widget
 */
class Phonefield extends FormWidgetBase
{

    /**
     * {@inheritDoc}
     */
    protected $defaultAlias = 'giveandgo_utils_phonefield';

    /**
     * {@inheritDoc}
     */
    public function init()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('phonefield');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['field'] = $this->formField;
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
    }

    /**
     * {@inheritDoc}
     */
    public function loadAssets()
    {
        $this->addCss('css/phonefield.css', 'giveandgo.utils');
        $this->addJs('js/jquery.maskedinput.min.js', 'giveandgo.utils');
        $this->addJs('js/phonefield.js', 'giveandgo.utils');
    }

    /**
     * {@inheritDoc}
     */
    public function getSaveValue($value)
    {
        return str_replace('-', '', $value);
    }

}
