<?php
namespace Giveandgo\Utils\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * json Form Widget
 */
class Json extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'giveandgo_utils_json';

    /**
     * @inheritDoc
     */
    public function init()
    {
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('json');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name']  = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
        $this->vars['field'] = $this->formField;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/json.css', 'giveandgo.utils');
        $this->addJs('js/json.js', 'giveandgo.utils');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return json_decode($value, true);
    }

    /**
     * @inheritDoc
     */
    public function getLoadValue()
    {
        $value = parent::getLoadValue();

        return json_encode($value, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }
}
