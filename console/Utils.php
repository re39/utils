<?php
namespace Giveandgo\Utils\Console;

use File;
use Illuminate\Console\Command;
use League\Csv\Reader as CsvReader;
use League\Csv\Statement;
use League\Csv\Writer as CsvWriter;
use RainLab\Translate\Models\Locale;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class Utils extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'gng:utils';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $command = implode(' ', (array) $this->argument('name'));
        $method  = 'util' . studly_case($command);

        if (!method_exists($this, $method)) {
            $this->error(sprintf('<error>Util command "%s" does not exist!</error>', $command));

            return;
        }

        $this->$method();
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::IS_ARRAY, 'A util command to perform.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['plugin', null, InputOption::VALUE_OPTIONAL, 'Plugin name to export|import language files'],
            ['file', null, InputOption::VALUE_OPTIONAL, 'File option'],
        ];
    }

    /**
     * @return null
     */
    protected function utilLangPopulate()
    {
        $file = 'lang.php';
        if ($this->option('file')) {
            $file = $this->option('file') . '.php';
        }

        $this->comment('Compiling language files...');

        if (class_exists('RainLab\Translate\Models\Locale')) {
            $locales        = Locale::lists('code');
            $fallbackLocale = Locale::getDefault()->code;
        } else {
            $locales        = ['en', 'ru'];
            $fallbackLocale = 'ru';
        }

        $plugin = $this->option('plugin');

        if (!$plugin) {
            throw new \Exception("Error Processing Request", 1);
        }

        $plugin     = explode('.', strtolower($plugin));
        $pluginPath = implode('/', $plugin);

        foreach ($locales as $locale) {

            /*
             * Generate messages
             */
            $fallbackPath = base_path() . '/plugins/' . $pluginPath . '/lang/' . $fallbackLocale . '/' . $file;
            $srcPath      = base_path() . '/plugins/' . $pluginPath . '/lang/' . $locale . '/' . $file;

            $messages = require $fallbackPath;
            if (File::isFile($srcPath) && $fallbackPath != $srcPath) {
                $messages = array_replace_recursive($messages, require $srcPath);
            }

            $contents = var_export($messages, true);
            $contents = '<?php' . PHP_EOL . 'return ' . $contents . ';';

            if (!File::isDirectory(dirname($srcPath))) {
                File::makeDirectory(dirname($srcPath));
            }

            File::put($srcPath, $contents);
            /*
             * Output notes
             */
            $publicDest = File::localToPublic(realpath(dirname($srcPath))) . '/' . basename($srcPath);

            $this->comment($locale . '/' . basename($srcPath));
            $this->comment(sprintf(' -> %s', $publicDest));
        }
    }

    /**
     * @return null
     */
    protected function utilLangExport()
    {
        $file    = 'lang.php';
        $csvfile = 'lang.csv';

        if ($this->option('file')) {
            $file    = $this->option('file') . '.php';
            $csvfile = $this->option('file') . '.csv';
        }

        $this->comment('Compiling language files...');

        if (class_exists('RainLab\Translate\Models\Locale')) {
            $locales        = Locale::lists('code');
            $fallbackLocale = Locale::getDefault()->code;
        } else {
            $locales        = ['en', 'ru'];
            $fallbackLocale = 'ru';
        }

        $plugin = $this->option('plugin');

        if (!$plugin) {
            throw new \Exception("Error Processing Request", 1);
        }

        $plugin     = explode('.', strtolower($plugin));
        $pluginPath = implode('/', $plugin);

        foreach ($locales as $locale) {

            /*
             * Generate messages
             */
            $fallbackPath = base_path() . '/plugins/' . $pluginPath . '/lang/' . $fallbackLocale . '/' . $file;
            $srcPath      = base_path() . '/plugins/' . $pluginPath . '/lang/' . $locale . '/' . $file;

            $messages = require $fallbackPath;

            if (File::isFile($srcPath) && $fallbackPath != $srcPath) {
                $messages = array_replace_recursive($messages, require $srcPath);
            }

            /*
             * Parse options
             */
            $options = [
                'fileName'  => implode('.', array_merge([$locale], $plugin, [$csvfile])),
                'delimiter' => ';',
                'enclosure' => '"',
                'escape'    => '\\',
            ];

            $filename = base_path() . '/plugins/' . $pluginPath . '/lang/' . filter_var($options['fileName'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

            if (!File::isFile($filename)) {
                File::put($filename, '');
            }

            /*
             * Prepare CSV
             */
            $csv = CsvWriter::createFromPath($filename, 'w');
            $csv->setDelimiter($options['delimiter']);
            $csv->setEnclosure($options['enclosure']);
            $csv->setEscape($options['escape']);

            $csv->insertOne(['code', 'value']);

            $messages = array_dot($messages);

            foreach ($messages as $key => $value) {
                $csv->insertOne([$key, $value]);
            }

            /*
             * Output notes
             */
            $this->comment($locale . '/' . basename($srcPath));
            $this->comment(sprintf(' -> %s', $filename));
        }
    }

    /**
     * @return null
     */
    protected function utilLangImport()
    {
        $file    = 'lang.php';
        $csvfile = 'lang.csv';

        if ($this->option('file')) {
            $file    = $this->option('file') . '.php';
            $csvfile = $this->option('file') . '.csv';
        }

        $this->comment('Importing language files...');

        if (class_exists('RainLab\Translate\Models\Locale')) {
            $locales        = Locale::lists('code');
            $fallbackLocale = Locale::getDefault()->code;
        } else {
            $locales        = ['es', 'en'];
            $fallbackLocale = 'en';
        }

        $plugin = $this->option('plugin');

        if (!$plugin) {
            throw new \Exception("Error Processing Request", 1);
        }

        $plugin     = explode('.', strtolower($plugin));
        $pluginPath = implode('/', $plugin);

        foreach ($locales as $locale) {

            /*
             * Generate messages
             */
            $fallbackPath = base_path() . '/plugins/' . $pluginPath . '/lang/' . $fallbackLocale . '/' . $file;
            $srcPath      = base_path() . '/plugins/' . $pluginPath . '/lang/' . $locale . '/' . $file;
            $messages     = require $fallbackPath;
            if (File::isFile($srcPath) && $fallbackPath != $srcPath) {
                $messages = array_replace_recursive($messages, require $srcPath);
            }

            /*
             * Parse options
             */
            $options = [
                'fileName'  => implode('.', array_merge([$locale], $plugin, [$csvfile])),
                'delimiter' => ';',
                'enclosure' => '"',
                'escape'    => '\\',
            ];

            $filename = base_path() . '/plugins/' . $pluginPath . '/lang/' . filter_var($options['fileName'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

            if (!File::isFile($filename)) {
                continue;
            }

            /*
             * Prepare CSV
             */
            $csv = CsvReader::createFromPath($filename, 'r');
            $csv->setDelimiter($options['delimiter']);
            $csv->setEnclosure($options['enclosure']);
            $csv->setEscape($options['escape']);

            $stmt    = (new Statement())->offset(1);
            $records = $stmt->process($csv);

            foreach ($records as $record) {
                if (!$code = array_get($record, 0)) {
                    continue;
                }
                $value = array_get($record, 1);
                array_set($messages, $code, $value);
            }

            $contents = var_export($messages, true);
            $contents = '<?php' . PHP_EOL . 'return ' . $contents . ';';

            if (!File::isDirectory(dirname($srcPath))) {
                File::makeDirectory(dirname($srcPath));
            }

            File::put($srcPath, $contents);

            /*
             * Output notes
             */
            $publicDest = File::localToPublic(realpath(dirname($srcPath))) . '/' . basename($srcPath);

            $this->comment($filename);
            $this->comment(sprintf(' -> %s', $publicDest));
        }
    }
}
