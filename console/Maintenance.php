<?php

namespace Giveandgo\Utils\Console;

use Cms\Models\MaintenanceSetting;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class Maintenance extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'utils:maintenance';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $switch = $this->argument('switch');

        $mode = ($switch == 'on');

        MaintenanceSetting::set('is_enabled', $mode);
        MaintenanceSetting::set('cms_page', '403');

        $this->info('Maintenance mode: ' . ($mode ? 'ON' : 'OFF'));
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['switch', InputArgument::REQUIRED, 'on|off'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
