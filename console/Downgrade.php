<?php

namespace Giveandgo\Utils\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use System\Classes\PluginManager;
use System\Classes\VersionManager;

class Downgrade extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'plugin:downgrade';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $pluginName = $this->argument('plugin');

        $pluginName = PluginManager::instance()->normalizeIdentifier($pluginName);
        if (!PluginManager::instance()->exists($pluginName)) {
            throw new \InvalidArgumentException(sprintf('Plugin "%s" not found.', $pluginName));
        }

        $this->info($pluginName);

        $manager = VersionManager::instance()->setNotesOutput($this->output);

        $version = $this->argument('version');

        $manager->removePlugin($pluginName, $version);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['plugin', InputArgument::REQUIRED, 'The name of the plugin. Eg: RainLab.Blog'],
            ['version', InputArgument::REQUIRED, 'Version to rollback to'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
