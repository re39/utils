<?php
namespace Giveandgo\Utils\Console;

use October\Rain\Scaffold\GeneratorCommand;
use October\Rain\Support\Str;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class CreateAjaxController extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gng:ajaxcontroller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new controller.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Controller';

    /**
     * A mapping of stub to generated file.
     *
     * @var array
     */
    protected $stubs = [
        'ajaxcontroller/_list_toolbar.stub' => 'controllers/{{lower_name}}/_list_toolbar.htm',
        'ajaxcontroller/config_form.stub'   => 'controllers/{{lower_name}}/config_form.yaml',
        'ajaxcontroller/config_list.stub'   => 'controllers/{{lower_name}}/config_list.yaml',
        'controller/config_relation.stub'   => 'controllers/{{lower_name}}/config_relation.yaml',
        'ajaxcontroller/_create_form.stub'  => 'controllers/{{lower_name}}/_create_form.htm',
        'ajaxcontroller/index.stub'         => 'controllers/{{lower_name}}/index.htm',
        'ajaxcontroller/_update_form.stub'  => 'controllers/{{lower_name}}/_update_form.htm',
        'ajaxcontroller/controller.stub'    => 'controllers/{{studly_name}}.php',
        'ajaxcontroller/js.stub'            => 'assets/js/{{lower_name}}.js',
        'controller/_relation_partial.stub' => 'controllers/{{lower_name}}/_relation_partial.htm',
    ];

    /**
     * Prepare variables for stubs.
     *
     * return @array
     */
    protected function prepareVars()
    {
        $pluginCode = $this->argument('plugin');

        $parts  = explode('.', $pluginCode);
        $plugin = array_pop($parts);
        $author = array_pop($parts);

        $controller = $this->argument('controller');

        /*
         * Determine the model name to use,
         * either supplied or singular from the controller name.
         */
        $model = $this->option('model');
        if (!$model) {
            $model = Str::singular($controller);
        }

        $rumodel       = $this->option('rumodel');
        $rumodelplural = $this->option('rumodelplural');

        return [
            'name'          => $controller,
            'model'         => $model,
            'rumodel'       => $rumodel,
            'rumodelplural' => $rumodelplural,
            'author'        => $author,
            'plugin'        => $plugin,
        ];
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['plugin', InputArgument::REQUIRED, 'The name of the plugin to create. Eg: RainLab.Blog'],
            ['controller', InputArgument::REQUIRED, 'The name of the controller. Eg: Posts'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['force', null, InputOption::VALUE_NONE, 'Overwrite existing files with generated ones.'],
            ['model', null, InputOption::VALUE_OPTIONAL, 'Define which model name to use, otherwise the singular controller name is used.'],
            ['rumodel', null, InputOption::VALUE_REQUIRED, 'Russian model name'],
            ['rumodelplural', null, InputOption::VALUE_REQUIRED, 'Russian model name in plural form'],
        ];
    }
}
