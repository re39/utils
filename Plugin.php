<?php
namespace Giveandgo\Utils;

use Backend;
use System\Classes\PluginBase;

/**
 * utils Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'utils',
            'description' => 'No description provided yet...',
            'author'      => 'giveandgo',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('gng.plugin', 'Giveandgo\Utils\Console\CreatePlugin');
        $this->registerConsoleCommand('gng.model', 'Giveandgo\Utils\Console\CreateModel');
        $this->registerConsoleCommand('gng.controller', 'Giveandgo\Utils\Console\CreateController');
        $this->registerConsoleCommand('gng.ajaxcontroller', 'Giveandgo\Utils\Console\CreateAjaxController');
        $this->registerConsoleCommand('gng.formwidget', 'Giveandgo\Utils\Console\CreateFormWidget');
        $this->registerConsoleCommand('gng.component', 'Giveandgo\Utils\Console\CreateComponent');
        $this->registerConsoleCommand('gng.command', 'Giveandgo\Utils\Console\CreateCommand');
        $this->registerConsoleCommand('gng.utils', 'Giveandgo\Utils\Console\Utils');
        $this->registerConsoleCommand('utils.maintenance', 'Giveandgo\Utils\Console\Maintenance');
        $this->registerConsoleCommand('plugin.downgrade', 'Giveandgo\Utils\Console\Downgrade');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Giveandgo\Utils\Components\SessionSettings' => 'sessionSettings',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'giveandgo.utils.some_permission' => [
                'tab'   => 'utils',
                'label' => 'Some permission',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'utils' => [
                'label'       => 'utils',
                'url'         => Backend::url('giveandgo/utils/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['giveandgo.utils.*'],
                'order'       => 500,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function registerMarkupTags()
    {
        return [
            'filters'   => [
                'autoLink'     => ['Giveandgo\Utils\Classes\TwigFilters', 'autoLinkText'],
                'gngsubstr'    => ['Giveandgo\Utils\Classes\TwigFilters', 'gngSubstr'],
                'gngNumEnding' => ['Giveandgo\Utils\Classes\TwigFilters', 'gngNumEnding'],
                'gngNum2str'   => ['Giveandgo\Utils\Classes\TwigFilters', 'gngNum2str'],
                'dateCase'     => ['Giveandgo\Utils\Classes\TwigFilters', 'rdate'],
                'weekday'      => function (int $number) {
                    $weekdays = [
                        'воскресенье',
                        'понедельник',
                        'вторник',
                        'среда',
                        'четверг',
                        'пятница',
                        'суббота',
                    ];

                    return array_get($weekdays, $number);
                },
                'nl2p'         => ['\Giveandgo\Utils\Classes\TwigFilters', 'nl2p'],
                'phone_view'   => function ($number) {
                    if (substr($number, 0, 1) == '9') {
                        return '8 ' . substr($number, 0, 3) . ' ' . substr($number, 3, 3) . ' ' . substr($number, 6, 2) . ' ' . substr($number, 8, strlen($number) - 8);
                    } elseif (substr($number, 0, 1) == '8') {
                        return '8 ' . substr($number, 0, 3) . ' ' . substr($number, 3, 3) . ' ' . substr($number, 6, 2) . ' ' . substr($number, 8, strlen($number) - 8);
                    } elseif (substr($number, 0, 1) == '4') {
                        return '8 (' . substr($number, 0, 3) . ') ' . substr($number, 3, 3) . ' ' . substr($number, 6, 2) . ' ' . substr($number, 8, strlen($number) - 8);
                    } else {
                        return '8 (' . substr($number, 0, 4) . ') ' . substr($number, 4, 2) . ' ' . substr($number, 6, 2) . ' ' . substr($number, 8, strlen($number) - 8);
                    }

                },
                'phone_call'   => function ($number) {
                    if (substr($number, 0, 1) == '8') {
                        return '8' . $number;
                    } else {
                        return '+7' . $number;
                    }

                },
            ],
            'functions' => [
                'has_plugin' => ['\Giveandgo\Utils\Classes\TwigFilters', 'checkPlugin'],
                'today'      => function () {
                    return \Carbon\Carbon::now();
                },
            ],
        ];
    }

    public function registerListColumnTypes()
    {
        return [
            'dropdown' => ['\Giveandgo\Utils\Classes\ColumnTypes', 'dropdown'],
        ];
    }

    public function registerFormWidgets()
    {
        return [
            '\Giveandgo\Utils\FormWidgets\Phonefield'  => 'phonefield',
            '\Giveandgo\Utils\FormWidgets\Ymapscoords' => 'map',
            '\Giveandgo\Utils\FormWidgets\Shopdesk'    => 'timetable',
            '\Giveandgo\Utils\FormWidgets\Arraylist'   => 'arraylist',
            '\Giveandgo\Utils\FormWidgets\Json'        => 'json',
        ];
    }
}
