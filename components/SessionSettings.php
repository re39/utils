<?php
namespace Giveandgo\Utils\Components;

use Cms\Classes\ComponentBase;
use October\Rain\Exception\ApplicationException;
use Session;

class SessionSettings extends ComponentBase
{
    /**
     * @var mixed
     */
    public $settings;

    public function componentDetails()
    {
        return [
            'name'        => 'SessionSettings Component',
            'description' => 'No description provided yet...',
        ];
    }

    public function defineProperties()
    {
        return [
            'settingNames' => [
                'description'       => 'Список настроек через зпт',
                'title'             => 'Список',
                'type'              => 'string',
                'validationPattern' => '^[0-9a-z,]+$',
                'validationMessage' => 'Допускаются только латиница без пробелов через запятую',
            ],
        ];
    }

    public function onRun()
    {
        $this->settings = $this->page['sessionSettings'] = $this->getSettings();
    }

    public function getSettingNames()
    {
        $settingNames = $this->property('settingNames');

        return explode(',', $settingNames);
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        $settingNames = $this->getSettingNames();

        $settings = [];

        foreach ($settingNames as $setting) {
            if (Session::has($setting)) {
                $settings[$setting] = Session::get($setting);
            }
        }

        return $settings;
    }

    /**
     * @param $setting
     */
    public function getSetting($setting)
    {
        if (!$setting) {
            return null;
        }

        if (Session::has($setting)) {
            return Session::get($setting);
        }
    }

    public function onSetting()
    {
        $setting = post('setting');

        if (!$setting || !in_array($setting, $this->getSettingNames())) {
            throw new ApplicationException('Error Processing Request', 1);
        }

        if (post('action') == 'get' && Session::has($setting)) {
            return Session::get($setting);
        } elseif (post('action') == 'set' && post('value')) {
            $value = post('value');
            Session::put($setting, $value);

            return Session::get($setting);
        } elseif (post('action') == 'toggle' && Session::has($setting)) {
            $value = !Session::get($setting);
            Session::put($setting, $value);

            return Session::get($setting);
        } elseif (post('action') == 'toggle' && !Session::has($setting)) {
            $value = true;
            Session::put($setting, $value);

            return Session::get($setting);
        }
    }
}
